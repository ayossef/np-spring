package org.example;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class BeanFactory {
    @Bean
    public NormalEngine getEngine(){
        return new NormalEngine();
    }

    @Bean
    public EcoEngine getEcoEngine(){
        return new EcoEngine();
    }
}
