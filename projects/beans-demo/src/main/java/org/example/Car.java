package org.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class Car {
    @Autowired
    private IEngine carEngine;
    public void startCar(){
        carEngine.start();
    }
    public void stopCar(){
        carEngine.stop();
    }

    public IEngine getCarEngine() {
        return carEngine;
    }

    public void setCarEngine(IEngine carEngine) {
        this.carEngine = carEngine;
    }
}
