package org.example;

import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

@Component
@Primary
public class EcoEngine implements  IEngine{
    public void start() {
        System.out.println("Starting Eco Engine");
    }

    public void stop() {
        System.out.println("Eco engine down");
    }
}
