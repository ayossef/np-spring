package org.example;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
        IEngine engineOne = new IEngine() {
            public void start() {
                System.out.println("Anonymous Engine Starting");
            }

            public void stop() {
                System.out.println("Anonymous engine Stopped");
            }
        };

        engineOne.start();
        engineOne.stop();

        IEngine engineTwo = new EcoEngine();
        engineTwo.start();
        engineTwo.stop();

        ApplicationContext springContext = new ClassPathXmlApplicationContext("beans.xml");
        IEngine engineThree = (IEngine) springContext.getBean("engine");
        engineThree.start();
        engineThree.stop();

        ApplicationContext beanFactory = new AnnotationConfigApplicationContext(BeanFactory.class);
        IEngine engineFive = beanFactory.getBean(NormalEngine.class);
        engineFive.start();
        engineFive.stop();

        ApplicationContext autoBeanFactory = new AnnotationConfigApplicationContext(AutoBeanFactory.class);
        IEngine engineSix = autoBeanFactory.getBean(SportsEngine.class);
        engineSix.start();
        engineSix.stop();

        Car myCar = autoBeanFactory.getBean(Car.class);
        myCar.startCar();
        myCar.stopCar();

    }
}
