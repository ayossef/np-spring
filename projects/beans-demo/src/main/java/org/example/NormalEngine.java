package org.example;

import org.springframework.stereotype.Component;

@Component
public class NormalEngine implements IEngine{
    public void start() {
        System.out.println("Starting normal engine");
    }

    public void stop() {
        System.out.println("Normal Engine Shut down");
    }
}
