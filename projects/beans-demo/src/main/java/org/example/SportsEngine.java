package org.example;

import org.springframework.stereotype.Component;

@Component
public class SportsEngine implements IEngine{
    @Override
    public void start() {
        System.out.println("Sports Engine Starting .. ");
    }

    @Override
    public void stop() {
        System.out.println("Sports Engine Stopping .. ");
    }
}
