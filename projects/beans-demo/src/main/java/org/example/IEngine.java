package org.example;

public interface IEngine {
    public void start();
    public void stop();
}
