package com.example.webclientdemo.service;

import com.example.webclientdemo.model.Product;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

@Service
public class ProductsService {

    private final WebClient webClient;


    public ProductsService(WebClient.Builder webClientBuilder) {
        this.webClient = webClientBuilder.baseUrl("https://localhost:8081").build();
    }

    public Product[] getProducts(){
        return webClient
                .get()
                .uri("/products/")
                .header("Content-Type","application/json")
                .header("Authorization","Bearer jwt token")
                .retrieve()
                .bodyToMono(Product[].class)
                .block();
    }
}
