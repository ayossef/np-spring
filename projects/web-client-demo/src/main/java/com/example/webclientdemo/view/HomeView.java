package com.example.webclientdemo.view;

import com.example.webclientdemo.model.Product;
import com.example.webclientdemo.service.ProductsService;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.router.Route;

@Route("")
public class HomeView extends VerticalLayout {
    public HomeView(ProductsService productsService) {

        Text title = new Text("");
        title.setText("Products List");
        add(title);

        Product[] allProducts = productsService.getProducts();
        Grid<Product> productGrid = new Grid<>(Product.class);
        productGrid.setItems(allProducts);
        add(productGrid);
    }
}
