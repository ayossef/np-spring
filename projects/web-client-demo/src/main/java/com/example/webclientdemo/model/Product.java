package com.example.webclientdemo.model;

import lombok.Data;

@Data
public class Product {
    private int id;
    private int price;
    private String name;

}
