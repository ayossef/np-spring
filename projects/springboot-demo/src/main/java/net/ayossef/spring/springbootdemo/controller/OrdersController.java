package net.ayossef.spring.springbootdemo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/orders")
public class OrdersController {
    @GetMapping("/welcome")
    public String welcome(){
        return "welcome to orders api controller";
    }
}
