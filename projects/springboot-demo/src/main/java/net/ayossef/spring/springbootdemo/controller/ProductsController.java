package net.ayossef.spring.springbootdemo.controller;

import net.ayossef.spring.springbootdemo.model.Products;
import net.ayossef.spring.springbootdemo.service.IProductsRepo;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

// http://localhost:8080/products/welcome
// http://localhost:8080/products/welcome

@RestController
@RequestMapping("/products")
public class ProductsController {
    final IProductsRepo productsRepo;

    public ProductsController(IProductsRepo productsRepo) {
        this.productsRepo = productsRepo;
    }

    @GetMapping(path = "/welcome")
    public String index(){
        return "welcome to products microservice";
    }
    @GetMapping("/first_product")
    public Products getProduct(){
        Products firstProduct = new Products();
        firstProduct.setId(5);
        firstProduct.setName("new product");
        firstProduct.setPrice(10);
        return firstProduct;
    }

    @GetMapping("/")
    public List<Products> getAll(){
        return productsRepo.findAll();
    }

    @GetMapping("/{productId}")
    public ResponseEntity<Optional<Products>> getProductById(@PathVariable(value = "productId") Integer productId){
        Optional<Products> resultsProduct = productsRepo.findById(productId);
        if(resultsProduct.isPresent()){
            return ResponseEntity.ok(resultsProduct);
        }else{
           return ResponseEntity.notFound().build();
        }
    }
    @DeleteMapping("/{productId}")
    public ResponseEntity<String> deleteProduct(@PathVariable(value = "productId") Integer productId){
        boolean doesExist = productsRepo.existsById(productId);
        if(doesExist){
            productsRepo.deleteById(productId);
            return ResponseEntity.ok("Deleted");
        }else{
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/{productId}")
    public ResponseEntity<Optional<Products>> updateProduct(@PathVariable(value = "productId") Integer productId,
                                                            @RequestBody Products updatedProduct){
        if(!productsRepo.existsById(productId)){
            return ResponseEntity.notFound().build();
        }else{
            Optional<Products> finalProduct =  productsRepo.findById(productId).map(currentProduct -> {
                currentProduct.setName(updatedProduct.getName());
                currentProduct.setPrice(updatedProduct.getPrice());
                return productsRepo.save(currentProduct);
            });
            if(finalProduct.isPresent()){
                return ResponseEntity.ok(finalProduct);
            }else{
                return ResponseEntity.internalServerError().build();
            }
        }
    }


    @GetMapping("/count")
    public ResponseEntity<HashMap<String,Long>> getCount(){
        HashMap<String, Long> result = new HashMap<>();
        result.put("count", productsRepo.count());
        ResponseEntity<HashMap<String,Long>> responseEntity = new ResponseEntity<>(result, HttpStatusCode.valueOf(200));
        return responseEntity;
    }




}
