package net.ayossef.spring.springbootdemo.service;

import net.ayossef.spring.springbootdemo.model.Products;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IProductsRepo extends JpaRepository<Products, Integer> {
}
