package net.ayossef.spring.springbootdemo.model;

import java.util.ArrayList;
import java.util.HashMap;

public class APIResponse {
    String message;
    Integer statusCode;
    ArrayList<Object> data;
    HashMap<String, String> flags;
}
