package org.example;

import org.example.cars.*;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );

        ApplicationContext context = new ClassPathXmlApplicationContext("engines.xml");
        Car testCar = new Car((Engine) context.getBean("normal"));
        testCar.startCar();
        testCar.stopCar();

        Car normal = new Car(new TurboEngine());
        normal.startCar();
        normal.stopCar();
//
//        Car myCar = new Car((Engine) context.getBean("eco"));
//        myCar.startCar();
//        myCar.stopCar();
//
//        Car yourCar = new Car((Engine) context.getBean("turbo"));
//        yourCar.startCar();
//        yourCar.stopCar();
//
//        Car elonCar = new Car((Engine) context.getBean("elec"));
//        elonCar.startCar();
//        elonCar.stopCar();
    }
}
