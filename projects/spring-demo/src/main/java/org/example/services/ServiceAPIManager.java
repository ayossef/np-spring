package org.example.services;

import org.example.services.model.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.util.ArrayList;


public class ServiceAPIManager {

    public String getServices(){

        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("repos.xml");

        IServiceRepo myRepo = (IServiceRepo) applicationContext.getBean("service_repo");
        ArrayList<Service> allServices = myRepo.getServices();
        StringBuilder sb = new StringBuilder();
        for (Service serivce:
             allServices) {
            sb.append(serivce.toString());
            sb.append("/n");
        }
        return sb.toString();
    }
}
