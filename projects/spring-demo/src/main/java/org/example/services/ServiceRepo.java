package org.example.services;

import org.example.services.model.Service;

import java.util.ArrayList;

public class ServiceRepo implements  IServiceRepo {

    String dbConnection = "";
    public ArrayList<Service> getServices() {
        Service sOne = new Service("checkCaseStatus", "398329732lkhd9823", 1);
        Service sTwo = new Service("updateCaseStatus", "089h32lndoihu32", 1);
        ArrayList<Service> allServices = new ArrayList<>();
        allServices.add(sOne);
        allServices.add(sTwo);
        return allServices;
    }

    public void setDbConnection(String dbConnection) {
        this.dbConnection = dbConnection;
    }
}
