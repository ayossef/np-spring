package org.example.services.model;

/*
This comments by hassan
 */
public class Service {
    String name;
    String uuid;
    int status;

    public Service(String name, String uuid, int status) {
        this.name = name;
        this.uuid = uuid;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public String getUuid() {
        return uuid;
    }

    public int getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "Service{" +
                "name='" + name + '\'' +
                ", uuid='" + uuid + '\'' +
                ", status=" + status +
                '}';



    }
}
