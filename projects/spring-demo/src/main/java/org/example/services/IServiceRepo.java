package org.example.services;

import org.example.services.model.Service;

import java.util.ArrayList;

public interface IServiceRepo {
    ArrayList<Service> getServices();
}
