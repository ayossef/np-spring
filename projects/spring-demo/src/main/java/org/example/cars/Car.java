package org.example.cars;
// Dubai Courts
public class Car {
    private Engine carEngine;

    public void startCar(){
        carEngine.start();
    }

    public void stopCar(){
        carEngine.stop();
    }
    public Car(Engine engine){
        this.carEngine = engine;
    }
}

