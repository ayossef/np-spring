package org.example.cars;

// Engine
public interface Engine {
    public void start();
    public void stop();
}
