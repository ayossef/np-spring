package org.example.cars;
// Eco Engine
public class EcoEngine implements Engine{
    @Override
    public void start() {
        System.out.println("Engine Started");
    }

    @Override
    public void stop() {
        System.out.println("Shut down");
    }
}
