package org.example.cars;

// Electrical Engine
public class ElectricalEngine implements Engine{
    @Override
    public void start() {
        System.out.println("Starting Elec");
    }

    @Override
    public void stop() {
        System.out.println("Stopping Elec");
    }
}
