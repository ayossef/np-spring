package com.example.demorest.dao;

import com.example.demorest.model.Employee;

import java.util.List;


public interface EmployeeManagementDAO {

	public void save(Employee employee);

	public int delete(int id);

	public Employee get(int id);

	public List<Employee> list();
}
