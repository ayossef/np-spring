package org.example;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );

        // 1. create context
        // get service api manager bean
        // call show services
        ApplicationContext context = new AnnotationConfigApplicationContext(BeanFactory.class);

        ServiceAPI serviceAPIManager = context.getBean(ServiceAPI.class);
        serviceAPIManager.showServices();
    }
}
