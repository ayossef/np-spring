package org.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ServiceAPIManager {

    // 3. add autowired
    @Autowired
    private IServiceRepo serviceProvider;

    public void showServices(){
        // 2. Complete this method to print all services
        serviceProvider.getServices();
    }
}
