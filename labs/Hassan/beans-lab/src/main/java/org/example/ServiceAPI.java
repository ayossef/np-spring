package org.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class ServiceAPI {
    @Autowired
    private IServiceRepo services;

    public IServiceRepo getServices() {
        return services;
    }

    public void setServices(IServiceRepo services) {
        this.services = services;
    }
    public void showServices(){
        ArrayList<Services> AllServices = services.getServices();
        for(Services currService :AllServices){
            System.out.println(currService.toString());
        }
    }


}
