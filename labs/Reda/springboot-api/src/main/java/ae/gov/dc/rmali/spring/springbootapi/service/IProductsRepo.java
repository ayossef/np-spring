package ae.gov.dc.rmali.spring.springbootapi.service;

import ae.gov.dc.rmali.spring.springbootapi.model.Products;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IProductsRepo extends JpaRepository<Products , Integer> {


}
