package ae.gov.dc.rmali.spring.springbootapi.model;


import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Generated;
/**/
@Entity
@Table(name="user_table")
public class User {


    @Id
    @GeneratedValue
    private Long  id;


    private String first_name;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }


    public String getName() {
        return first_name;
    }

    public void setName(String name) {
        this.first_name = name;
    }


}
