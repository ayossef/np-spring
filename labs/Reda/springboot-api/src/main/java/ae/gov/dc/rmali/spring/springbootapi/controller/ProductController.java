package ae.gov.dc.rmali.spring.springbootapi.controller;

import ae.gov.dc.rmali.spring.springbootapi.model.Products;
import ae.gov.dc.rmali.spring.springbootapi.service.IProductsRepo;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping( "/products")
public class ProductController {
    final IProductsRepo productsRepo ;

    public ProductController(IProductsRepo productsRepo) {
        this.productsRepo = productsRepo;
    }

    @GetMapping(path  = "/welcome")
    public String index(){
        return "Welcome to Product microservice";
    }

    @GetMapping(path = "/first_product")
    public Products getProduct(){
        Products firstProduct = new Products();
        firstProduct.setId(5);
        firstProduct.setName("new iPhone product");
        firstProduct.setPrice(6000);
        return firstProduct;
    }

    @GetMapping(path="/all")
    public List<Products> getAll(){

         return productsRepo.findAll();

    }


    @GetMapping("/{productId}")
    public ResponseEntity<Optional<Products>> getProductById(@PathVariable (value = "productId") Integer productId) {

        Optional<Products> resultProdcut = productsRepo.findById(productId) ;

        if(resultProdcut.isPresent()){

            return ResponseEntity.ok(resultProdcut) ;
        }
        else {
            return ResponseEntity.notFound().build();
        }
    }

    @DeleteMapping("/{productId}")
    public ResponseEntity<String> deleteProductById(@PathVariable (value = "productId") Integer productId) {

        Boolean doesExists = productsRepo.existsById(productId) ;

        if(doesExists){
            productsRepo.deleteById(productId);
            return ResponseEntity.ok("Deleted");
        }
        else {
            return ResponseEntity.notFound().build();
        }
    }

    @PutMapping("/{productId}")
    public ResponseEntity<Optional<Products>> updateProduct(@PathVariable(value = "productId") Integer productId,
                                                            @RequestBody Products updatedProduct){
        if(!productsRepo.existsById(productId)){
            return ResponseEntity.notFound().build();
        }else{
            Optional<Products> finalProduct =  productsRepo.findById(productId).map(currentProduct -> {
                currentProduct.setName(updatedProduct.getName());
                currentProduct.setPrice(updatedProduct.getPrice());
                return productsRepo.save(currentProduct);
            });
            if(finalProduct.isPresent()){
                return ResponseEntity.ok(finalProduct);
            }else{
                return ResponseEntity.internalServerError().build();
            }
        }
    }



    // comment
    @GetMapping("/count")
    public ResponseEntity<HashMap<String,Long>> getCount(){
        HashMap<String, Long> result = new HashMap<>();
        result.put("Count", productsRepo.count());
        ResponseEntity<HashMap<String,Long>> responseEntity = new ResponseEntity<>(result, HttpStatusCode.valueOf(200));
        return responseEntity;
    }

}

