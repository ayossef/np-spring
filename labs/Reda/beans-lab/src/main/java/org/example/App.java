package org.example;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );

        // 1. create context
        ApplicationContext autoBeanSerive = new AnnotationConfigApplicationContext(AutoBeanService.class);
        // get service api manager bean
        ServiceAPI serviceAPI = autoBeanSerive.getBean(ServiceAPI.class) ;
        // call show services
        serviceAPI.showServices();

    }
}
