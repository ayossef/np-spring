package org.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import java.sql.SQLOutput;
import java.util.ArrayList;

@Component
public class ServiceAPI {
    @Autowired
    private  IServiceRepo getService;

    public void setGetService(IServiceRepo getService) {
        this.getService = getService;
    }

    public IServiceRepo getGetService() {
        return getService;
    }

    public void showServices(){
        // 2. Complete this method to print all services
        ArrayList<Services> allServices = getService.getServices();
        for (Services currService : allServices
             ) {
            System.out.println(currService);
        }

    }
}
