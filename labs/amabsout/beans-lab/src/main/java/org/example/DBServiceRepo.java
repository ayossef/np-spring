package org.example;

import java.util.ArrayList;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

// primary
@Component
@Primary
public class DBServiceRepo implements IServiceRepo{
    public ArrayList<Services> getServices() {
        ArrayList<Services> allServices = new ArrayList<Services>();
        allServices.add(new Services("db-case-check","case-904",1));
        allServices.add(new Services("db-case-update","case-974",1));
        return allServices;
    }
}
