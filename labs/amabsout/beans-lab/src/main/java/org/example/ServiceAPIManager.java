package org.example;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Component
public class ServiceAPIManager {

    // 3. add autowired
    @Autowired
    private IServiceRepo serviceProvider;

    public void showServices(){
        // 2. Complete this method to print all services
        ArrayList<Services> allServices =
        serviceProvider.getServices();
        for (Services s : allServices) {
            System.out.println(s);
        }
    }
}
