package org.example;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );

        // 1. create context
        // get service api manager bean
        // call show services

        ApplicationContext autoBeanFactory = new AnnotationConfigApplicationContext(AutoBeanFactory.class);
        ServiceAPIManager myService = autoBeanFactory.getBean(ServiceAPIManager.class);
        myService.showServices();

    }
}
