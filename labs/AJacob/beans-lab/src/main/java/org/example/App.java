package org.example;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );

        ApplicationContext beanFactory = new AnnotationConfigApplicationContext(BeanFactory.class);
        ServiceAPIManager serviceAPIManager=beanFactory.getBean(ServiceAPIManager.class);
        serviceAPIManager.showServices();
        //engineFive.start();
        //engineFive.stop();

        // 1. create context
        // get service api manager bean
        // call show services
    }
}
