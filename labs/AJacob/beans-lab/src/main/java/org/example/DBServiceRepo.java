package org.example;

import java.util.ArrayList;

// primary
public class DBServiceRepo implements IServiceRepo{
    public ArrayList<Services> getServices() {
        ArrayList<Services> allServices = new ArrayList<Services>();
        allServices.add(new Services("db-case-check","case-904",1));
        allServices.add(new Services("db-case-update","case-974",1));
        return allServices;
    }
}
