package org.example;

import com.sun.java.accessibility.util.AccessibilityListenerList;

import java.util.ArrayList;

public class XMLServiceRepo implements IServiceRepo{

    public ArrayList<Services> getServices() {
        ArrayList<Services> allServices = new ArrayList<Services>();
        allServices.add(new Services("xml-case-check","case-304",1));
        allServices.add(new Services("xml-case-update","case-374",1));
        return allServices;
    }
}
