package org.example;

import java.util.ArrayList;

public class ServiceAPIManager {

    // 3. add autowired
    private IServiceRepo serviceProvider;

    public void showServices(){
        // 2. Complete this method to print all services
        ArrayList<Services> AllServices= serviceProvider.getServices();
        for (Services s:AllServices
             ) {
            System.out.println(s);
            
        }
    }
}
