package org.example;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );

        // 1. create context
        ApplicationContext beanFactory = new AnnotationConfigApplicationContext(BeanFactory.class);
        // get service api manager bean
        ServiceAPIManager SAM = beanFactory.getBean(ServiceAPIManager.class);

        // call show services
        SAM.showServices();


    }
}
