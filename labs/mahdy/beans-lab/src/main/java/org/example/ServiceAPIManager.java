package org.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class ServiceAPIManager {

    @Autowired
    private IServiceRepo serviceProvider;

    public void showServices(){
        // 2. Complete this method to print all services
        ArrayList<Services> AllServices = serviceProvider.getServices();

        for (Services S:AllServices) {
            System.out.println(S);
                            }
    }

    public IServiceRepo getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(IServiceRepo serviceProvider) {
        this.serviceProvider = serviceProvider;
    }
}
