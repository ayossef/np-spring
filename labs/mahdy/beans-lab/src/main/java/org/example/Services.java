package org.example;

import org.springframework.stereotype.Component;

public class Services {
    String name;
    String uuid;
    int status;

    public Services(String name, String uuid, int status) {
        this.name = name;
        this.uuid = uuid;
        this.status = status;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Services{" +
                "name='" + name + '\'' +
                ", uuid='" + uuid + '\'' +
                ", status=" + status +
                '}';
    }
}
