package org.example;

public class ServiceAPIManager {

    // 3. add autowired
    private IServiceRepo serviceProvider;

    public void showServices(){
        // 2. Complete this method to print all services
        serviceProvider.getServices();
    }
}
