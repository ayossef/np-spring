package org.example;

import java.util.ArrayList;

public interface IServiceRepo {
    public ArrayList<Services> getServices();
}
