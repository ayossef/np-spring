package org.example;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );

        // 1. create context
        ApplicationContext autoBeanFactory = new AnnotationConfigApplicationContext(AutoBean.class);
        ServiceAPIManager Services1 = autoBeanFactory.getBean(ServiceAPIManager.class);
        Services1.showServices();

        // get service api manager bean
        // call show services
    }

}
