package org.example;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class ServiceAPIManager {

    // 3. add autowired
@Autowired
    private IServiceRepo serviceProvider;

    public void showServices(){
        // 2. Complete this method to print all services
        ArrayList<Services> ALlServices = serviceProvider.getServices();
        for (Services s:ALlServices
             ) {
            System.out.println(s);
        }

    }

    public IServiceRepo getServiceProvider() {
        return serviceProvider;
    }

    public void setServiceProvider(IServiceRepo serviceProvider) {
        this.serviceProvider = serviceProvider;
    }
}
