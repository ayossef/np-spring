package dc.gov.asharif.spring.springbootdemo.controller;

import dc.gov.asharif.spring.springbootdemo.model.ProductsEntity;
import dc.gov.asharif.spring.springbootdemo.services.IProductRepo;
import org.springframework.http.HttpStatusCode;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

@RestController

@RequestMapping("Products")
public class ProductsController {
    final
    IProductRepo productRepo;

    public ProductsController(IProductRepo productRepo) {
        this.productRepo = productRepo;
    }

    @GetMapping("/Abdo")
    public String index() {
        return "Welcome abdo";
    }

    @GetMapping("/first_product")
    public ProductsEntity getProduct() {
        ProductsEntity firstProduct = new ProductsEntity();
        firstProduct.setId(5);
        firstProduct.setName("new product");
        firstProduct.setPrice(10);
        return firstProduct;
    }
    @GetMapping("/{productId}")
    public ResponseEntity<Optional<ProductsEntity>> getProductById(@PathVariable(value = "productId") Integer productId){
        Optional<ProductsEntity> resultsProduct = productRepo.findById(productId);
        if(resultsProduct.isPresent()){
            return ResponseEntity.ok(resultsProduct);
        }else{
            return ResponseEntity.notFound().build();
        }
    }
    @GetMapping("/")
    public List<ProductsEntity> getAll() {
        return productRepo.findAll();

    }
    @GetMapping("/count")
    public ResponseEntity<HashMap<String,Long>> getCount(){
        HashMap<String, Long> result = new HashMap<>();
        result.put("Count", productRepo.count());
        ResponseEntity<HashMap<String,Long>> responseEntity = new ResponseEntity<>(result, HttpStatusCode.valueOf(200));
        return responseEntity;
    }
    @DeleteMapping("/{productId}")
    public ResponseEntity<String> deleteProduct(@PathVariable(value = "productId") Integer productId){
        boolean doesExist = productRepo.existsById(productId);
        if(doesExist){
            productRepo.deleteById(productId);
            return ResponseEntity.ok("Deleted");
        }else{
            return ResponseEntity.notFound().build();
        }
    }
    @PutMapping("/{productId}")
    public ResponseEntity<Optional<ProductsEntity>> updateProduct(@PathVariable(value = "productId") Integer productId,
                                                            @RequestBody ProductsEntity updatedProduct){
        if(!productRepo.existsById(productId)){
            return ResponseEntity.notFound().build();
        }else{
            Optional<ProductsEntity> finalProduct =  productRepo.findById(productId).map(currentProduct -> {
                currentProduct.setName(updatedProduct.getName());
                currentProduct.setPrice(updatedProduct.getPrice());
                return productRepo.save(currentProduct);
            });
            if(finalProduct.isPresent()){
                return ResponseEntity.ok(finalProduct);
            }else{
                return ResponseEntity.internalServerError().build();
            }
        }
    }

}
