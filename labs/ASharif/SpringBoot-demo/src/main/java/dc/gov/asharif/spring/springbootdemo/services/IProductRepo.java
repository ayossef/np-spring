package dc.gov.asharif.spring.springbootdemo.services;

import dc.gov.asharif.spring.springbootdemo.model.ProductsEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface IProductRepo extends JpaRepository<ProductsEntity,Integer> {


}
